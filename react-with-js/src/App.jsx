import { useState } from "react";
import "./App.css";
import Modal from "../src/assets/components/Modal";

function App() {
  const [showModal, setShowModal] = useState(false);
  const [gifts, setGifts] = useState([]);

  const handleModalOpen = () => {
    setShowModal(true);
  };
  const handleModalClose = () => {
    setShowModal(false);
  };
  const handleModalSave = (gift) => {
    setGifts([...gifts, gift]);
  };
  return (
    <div className="App">
      {showModal && (
        <Modal onClose={handleModalClose} onSave={handleModalSave} />
      )}
      <h1>My Birthday Gifts</h1>
      <div className="cards">
        <button onClick={handleModalOpen}>Add a Gift</button>
      </div>
      <div className="cards-gift">
        {gifts.map((gift, index) => (
          <div className="card-gift" key={index}>
            <img src={gift.image} alt="" />
            <h1>{gift.name}</h1>
            <p>${gift.value}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
