import { useState } from "react";

export default function Modal({
  onClose: handleModalClose,
  onSave: handleSave,
}) {
  const [name, setName] = useState("");
  const [value, setValue] = useState("");
  const [image, setImage] = useState("");

  const handleModalSave = () => {
    handleSave({ name, value, image });
    setName("");
    setValue("");
    setImage("");
    handleModalClose();
  };

  return (
    <div className="backdrop">
      <div className="modal">
        <input
          placeholder="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <input
          placeholder="Value"
          type="number"
          value={value}
          onChange={(e) => setValue(e.target.value)}
        />
        <input
          placeholder="Image"
          value={image}
          onChange={(e) => setImage(e.target.value)}
        />
        <button onClick={handleModalSave}>Save</button>
        <button onClick={handleModalClose}>Close</button>
      </div>
    </div>
  );
}
