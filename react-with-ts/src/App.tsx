import { useState } from "react";
import "./App.css";
import Modal from "./components/Modal";

export interface Gift {
  id: string;
  name: string;
  value: number;
  image: string;
}

function App() {
  const [showModal, setShowModal] = useState(false);
  const [gifts, setGifts] = useState<Gift[]>([]);

  const handleModalOpen = () => {
    setShowModal(true);
  };
  const handleModalClose = () => {
    setShowModal(false);
  };
  const handleModalSave = (gift: Gift) => {
    setGifts([...gifts, gift]);
  };
  return (
    <div className="App">
      {showModal && (
        <Modal onClose={handleModalClose} onSave={handleModalSave} />
      )}
      <h1>My Birthday Gifts</h1>
      <div className="cards">
        <button onClick={handleModalOpen}>Add a Gift</button>
      </div>
      <div className="cards-gift">
        {gifts.map((gift) => (
          <div className="card-gift" key={gift.id}>
            <img src={gift.image} alt="" />
            <h1>{gift.name}</h1>
            <p>${gift.value}</p>
          </div>
        ))}
      </div>
    </div>
  );
}

export default App;
